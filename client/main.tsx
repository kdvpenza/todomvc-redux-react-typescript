import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Store, createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import App from './typeahead/components/App';
import typeahead from './typeahead';

const rootReducer = combineReducers({
    typeahead
});

const initialState = {};

const store: Store<any> = createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk)
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);