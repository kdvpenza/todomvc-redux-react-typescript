export type ItemModel = {
  id: number;
  text: string;
};

export type ItemsModel = ItemModel[];

export type FetchStatusModel = {
  isFetching: boolean;
  errorText: string;
};

export type StoreModel = {
  searchPhrase: string,
  allItems: ItemsModel,
  filteredItems: ItemsModel,
  fetchStatus: FetchStatusModel
};