import {
  handleActions,
  Action
} from 'redux-actions';

import {
  model,
  settings
} from './';

import {
  UPDATE,
  CLEAR,
  DATA_REQUEST,
  DATA_SUCCESS,
  DATA_ERROR
} from './constants/ActionTypes';

import * as dreamjs from 'dreamjs';

const initialState: model.StoreModel = {
  searchPhrase: '',
  allItems: getInitialData(),
  filteredItems: [],
  fetchStatus: {
    isFetching: false,
    errorText: settings.EMPTY_STRING
  }
};

function getInitialData() : model.ItemsModel {
  dreamjs.customType('incrementalId', helper => helper.previousItem ? helper.previousItem.id + 1 : 1);
  return dreamjs
      .schema({
        id: 'incrementalId',
        text: 'name'
      })
      .generateRnd(settings.INITIAL_DATA_COUNT)
      .output();
}

function filterData(data: model.ItemsModel, text: string) : model.ItemsModel {
  return data
      .filter(value => {
        // find all matches
        return value.text.toLowerCase().indexOf(text.toLowerCase()) !== -1;
      })
      .slice(0, settings.SEARCH_RESULTS_COUNT) // Limit count
      .map((value, index) => {
        // Convert to ItemsModel
        return {
          id: index,
          text: value.text
        };
      });
}

export default handleActions<model.StoreModel>({

  [UPDATE]: (state: model.StoreModel, action: Action<string>): model.StoreModel => {
    return {
      ...state,
      searchPhrase: action.payload,
      filteredItems: filterData(state.allItems, action.payload)
    };
  },

  [CLEAR]: (state: model.StoreModel): model.StoreModel => {
    return {
      ...state,
      filteredItems: []
    };
  },

  [DATA_REQUEST]: (state: model.StoreModel): model.StoreModel => {
    return {
      ...state,
      fetchStatus: {
        ...state.fetchStatus,
        isFetching: true,
      }
    }
  },

  [DATA_SUCCESS]: (state: model.StoreModel, action: Action<model.ItemsModel>): model.StoreModel => {
    return {
      ...state,
      allItems: action.payload,
      fetchStatus: {
        ...state.fetchStatus,
        isFetching: false,
      }
    }
  },

  [DATA_ERROR]: (state: model.StoreModel, action: Action<string>): model.StoreModel => {
    return {
      ...state,
      fetchStatus: {
        errorText: action.payload,
        isFetching: false,
      }
    }
  }

}, initialState);
