// Max rendering items
export const SEARCH_RESULTS_COUNT = 5;

// ID of unselected item
export const UNSELECTED_ITEM_ID = -1;

// Count of local random data
export const INITIAL_DATA_COUNT = 10000;

// Empty string
export const EMPTY_STRING = '';

// AJAX request timeout (in milliseconds)
export const AJAX_TIMEOUT = 1000;

// REST-service URL
export const REST_URL = 'http://services.groupkt.com/country/get/all';