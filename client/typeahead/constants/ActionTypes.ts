export const UPDATE         = 'UPDATE';
export const CLEAR          = 'CLEAR';
export const DATA_REQUEST   = 'DATA_REQUEST';
export const DATA_SUCCESS   = 'DATA_SUCCESS';
export const DATA_ERROR     = 'DATA_ERROR';