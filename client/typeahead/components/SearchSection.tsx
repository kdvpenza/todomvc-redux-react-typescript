import * as React from 'react';
import { ItemsModel } from '../model';
import './css/SearchSection.css';

interface SearchSectionProps {
    text: string;
    resultItems: ItemsModel;
    update: (text: string) => void;
    clear: () => void;
    keyDown: (e, hint: string) => void;
}

class SearchSection extends React.Component<SearchSectionProps, void> {
    render() {
        const { text } = this.props;
        return (
            <div className="search-section">
                <input
                    className="search-input transparent"
                    type="text"
                    value={this.getHint(text)}/>
                <input
                    className="search-input"
                    type="text"
                    placeholder="Search..."
                    value={text}
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    onKeyDown={this.handleKeyDown}/>
            </div>
        );
    }

    private handleChange = e => {
        this.doUpdate(e.target.value);
    };

    private handleBlur = () => {
        const { clear } = this.props;
        clear();
    };

    private handleFocus = e => {
        this.doUpdate(e.target.value);
    };

    private handleKeyDown = e => {
        const { text, keyDown } = this.props;
        keyDown(e, this.getHint(text));
    };

    private doUpdate = (inputText: string) => {
        const { update } = this.props;
        update(inputText);
    };

    private getHint = (inputText: string) : string => {
        const { resultItems } = this.props;
        let hint: string = '';
        if (inputText.length && resultItems.length > 0) {
            let firstResultTxt: string = resultItems[0].text;
            if (firstResultTxt.toLowerCase().indexOf(inputText.toLowerCase()) === 0) {
                hint = firstResultTxt.toLowerCase().replace(inputText.toLowerCase(), inputText);
            }
        }
        return hint;
    };
}

export default SearchSection;