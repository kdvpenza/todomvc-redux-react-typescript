import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';
import './css/App.css';

import {
  SearchSection,
  ResultsSection,
  model,
  settings,
  updateAction,
  clearAction,
  requireData
} from '../';

interface AppProps {
  searchPhrase: string;
  resultItems: model.ItemsModel;
  isDataFetching: boolean;
  fetchErrorText: string;
  dispatch: Dispatch<{}>;
}

interface AppState {
  selectedItemId: number;
  selectedItemText: string;
}

class App extends React.Component<AppProps, AppState> {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedItemId: settings.UNSELECTED_ITEM_ID,
      selectedItemText: settings.EMPTY_STRING
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(requireData());
  }

  componentWillReceiveProps(nextProps) {
    // reset AppState in case update() or clear() called
    if (nextProps.searchPhrase !== this.props.searchPhrase ||
        nextProps.resultItems !== this.props.resultItems) {
      this.updateState(settings.UNSELECTED_ITEM_ID, settings.EMPTY_STRING);
    }
  }

  render() {
    const { searchPhrase, resultItems, isDataFetching, fetchErrorText, dispatch } = this.props;
    const { selectedItemId, selectedItemText } = this.state;
    let searchComponent =
        <SearchSection
            text={selectedItemText || searchPhrase}
            resultItems={resultItems}
            update={(text: string) => dispatch(updateAction(text))}
            clear={() => dispatch(clearAction())}
            keyDown={(e, hint: string) => {this.handleSearchSectionKeyDown(e, hint)}}/>;
    let resultsComponent =
        <ResultsSection
            resultItems={resultItems}
            selectedItemId={selectedItemId}/>;
    let components = isDataFetching ?
        <div className="info">Loading data. Please wait...</div> :
        <div>{searchComponent}{resultsComponent}</div>;
    return (
        <div className="app"
             onMouseDown={this.handleMouseDown}>
          <h1>typeahead.js</h1>
          <div className={fetchErrorText.length > 0 ? 'info' : 'hidden'}>
            Error while data loading:<br/>
            {fetchErrorText}<br/>
            Using local random data
          </div>
          {components}
        </div>
    );
  }

  private handleMouseDown = e => {
    const { dispatch } = this.props;
    if (e.target.className === 'result-item') {
      dispatch(updateAction(e.target.textContent));
    }
  };

  private handleSearchSectionKeyDown = (e, hint: string) => {
    const { dispatch, resultItems, searchPhrase } = this.props;
    const { selectedItemId, selectedItemText } = this.state;
    if (e.target.className === 'search-input') {
      switch (e.keyCode) {
        case 9: // <Tab>
          if (selectedItemId === settings.UNSELECTED_ITEM_ID && resultItems.length > 0) {
            e.preventDefault();
            dispatch(updateAction(resultItems[0].text));
            if (hint && hint === searchPhrase) {
              dispatch(clearAction());
            }
            break;
          }
          // fall-through
        case 13: // <Enter>
          if (selectedItemId === settings.UNSELECTED_ITEM_ID) {
            return;
          }
          dispatch(updateAction(selectedItemText));
          // fall-through
        case 27: // <Esc>
          e.preventDefault();
          dispatch(clearAction());
          break;
        case 38: // <Up>
          e.preventDefault();
          for (let i: number = 0; i < resultItems.length; i++) {
            if (resultItems[i].id === selectedItemId) {
              if (i !== 0) {
                let item: model.ItemModel = resultItems[i - 1];
                this.updateState(item.id, item.text);
              }
              else {
                this.updateState(settings.UNSELECTED_ITEM_ID, settings.EMPTY_STRING);
              }
              return;
            }
          }
          // select last item
          if (resultItems.length > 0) {
            let item: model.ItemModel = resultItems[resultItems.length - 1];
            this.updateState(item.id, item.text);
          }
          break;
        case 40: // <Down>
          e.preventDefault();
          for (let i: number = 0; i < resultItems.length; i++) {
            if (resultItems[i].id === selectedItemId) {
              if (i !== resultItems.length - 1) {
                let item: model.ItemModel = resultItems[i + 1];
                this.updateState(item.id, item.text);
              }
              else {
                this.updateState(settings.UNSELECTED_ITEM_ID, settings.EMPTY_STRING);
              }
              return;
            }
          }
          // select first item
          if (resultItems.length > 0) {
            let item: model.ItemModel = resultItems[0];
            this.updateState(item.id, item.text);
          }
          break;
      }
    }
  };

  private updateState = (id: number, text: string) => {
    this.setState({
      selectedItemId: id,
      selectedItemText: text
    });
  };
}

const mapStateToProps = state => ({
  searchPhrase: state.typeahead.searchPhrase,
  resultItems: state.typeahead.filteredItems,
  isDataFetching: state.typeahead.fetchStatus.isFetching,
  fetchErrorText: state.typeahead.fetchStatus.errorText
});

export default connect(mapStateToProps)(App);
