import * as React from 'react';
import * as classNames from 'classnames';
import { ItemsModel } from '../model';
import './css/ResultsSection.css';

interface ResultsSectionProps {
    resultItems: ItemsModel;
    selectedItemId: number;
}

class ResultsSection extends React.Component<ResultsSectionProps, void> {
    render() {
        const { resultItems, selectedItemId } = this.props;
        return (
            <div className="results-section">
                <ul className="results-list">
                    {resultItems.map(item =>
                        <li className={
                            classNames({
                                "result-item": true,
                                selected: (item.id === selectedItemId)
                            })}
                            key={item.id}>
                            {item.text}
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}

export default ResultsSection;