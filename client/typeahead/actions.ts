import { createAction } from 'redux-actions';
import "es6-promise/auto";
import axios from 'axios';

import {
  UPDATE,
  CLEAR,
  DATA_REQUEST,
  DATA_SUCCESS,
  DATA_ERROR
} from './constants/ActionTypes';

import {
    model,
    settings
} from './';

const updateAction = createAction<string, string>(
    UPDATE,
    (text: string) => text
);

const clearAction = createAction<void>(
    CLEAR,
    () => { }
);

const dataRequestAction = createAction<void>(
    DATA_REQUEST,
    () => { }
);

const dataSuccessAction = createAction<model.ItemsModel, model.ItemsModel>(
    DATA_SUCCESS,
    (items: model.ItemsModel) => items
);

const dataErrorAction = createAction<string, string>(
    DATA_ERROR,
    (text: string) => text
);

// Get data via REST
const requireData = function() {
  // Use thunk-middleware
  return (dispatch) => {
      dispatch(dataRequestAction());
      axios.get(settings.REST_URL, {timeout: settings.AJAX_TIMEOUT})
          .then((response) => {
                // Assuming that response.status always 200 here
                try {
                    dispatch(
                        dataSuccessAction(response.data.RestResponse.result.map((item, index) => {
                            return {
                                id: index,
                                text: item.name
                            }
                        }))
                    );
                }
                catch (error) {
                    //fall-through to Promise.catch
                    throw (error);
                }
          })
          .catch((error) => {
                if (error.response) {
                    dispatch(dataErrorAction(error.response.status + ': ' + error.response.statusText));
                }
                else {
                    dispatch(dataErrorAction(error.message));
                }
          });
  }
};

export {
  updateAction,
  clearAction,
  requireData
};
